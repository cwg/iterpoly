Welcome to Iterpoly's documentation!
====================================

.. automodule:: iterpoly
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
