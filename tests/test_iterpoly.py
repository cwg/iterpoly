from fractions import Fraction
import iterpoly as ip


def test_consistency():
    # Represent 3 - 7 x^2 + 16 x^4
    poly = [3, 0, -7, 0, 16]
    poly2 = list(ip.diff(ip.integrate(map(Fraction, poly))))
    assert poly == poly2
