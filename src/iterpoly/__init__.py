"""Algebra on polynomials represented as iterables."""


def diff(poly):
    """Differentiate a polynomial.
    """
    it = enumerate(poly)
    next(it)
    for i, c in it:
        yield c * i


def integrate(poly):
    """Integrate a polynomial.
    """
    yield 0
    for i, c in enumerate(poly):
        yield c / (i + 1)
