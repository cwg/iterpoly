Iterpoly
========

Iterpoly can perform some simple algebra on polynomials that are
represented as Python iterables.  However, its main purpose is serving
as an example of packaging a (numerical) Python library.

Usage example
-------------

::

    >>> import iterpoly as ip
    >>> print(list(ip.diff([2, 1, 0, 4])))
    [1, 0, 12]
    >>> print(list(ip.diff([2, 7, 0, 4])))
    [7, 0, 12]
    >>> print(list(ip.integrate([7, 0, 12])))
    [0, 7.0, 0.0, 4.0]
